<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$adult = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->AdultContentDetection($tweet);
	if (!isset($adult[$result])) {
	    $adult[$result] = 0;
	}
	$adult[$result]++;
}

unset($DatumboxAPI);

print json_encode($adult);